# Loosely based on suite/branding/Makefile.in

DEPTH		= ../..
topsrcdir	= @top_srcdir@
srcdir		= @srcdir@
VPATH		= @srcdir@

include $(DEPTH)/config/autoconf.mk

ICON_SUFFIX=.png

DESKTOP_ICONS = \
	abcardWindow \
	ablistWindow \
	addressbookWindow \
	bmPropsWindow \
	bookmark-window \
	default \
	downloadManager \
	editorWindow \
	findBookmarkWindow \
	findHistoryWindow \
	history-window \
	JSConsoleWindow \
	main-window \
	messengerWindow \
	msgcomposeWindow \
	$(NULL)

GARBAGE	+= $(DIST)/bin/chrome/icons/default/iceape.png

DESKTOP_ICONS_SMALL = $(patsubst %,%16,$(DESKTOP_ICONS))
DESKTOP_ICONS_LARGE = $(patsubst %,%48,$(DESKTOP_ICONS))

DESKTOP_ICON_FILES = $(addsuffix $(ICON_SUFFIX), $(DESKTOP_ICONS) $(DESKTOP_ICONS_SMALL) $(DESKTOP_ICONS_LARGE)) iceape.png

CHROME_DEPS = content/aboutRights.xhtml content/about.png content/icon64.png

ALL_FILES = $(addprefix icons/gtk/,$(DESKTOP_ICON_FILES)) $(CHROME_DEPS)

$(ALL_FILES)::
	[ -d $(dir $@) ] || mkdir -p $(dir $@)

content/about.png:: $(srcdir)/iceape_logo_plain.svg
	rsvg-convert -w 300 -h 280 -o $@ $^

content/icon64.png:: $(srcdir)/iceape_icon_plain.svg
	rsvg-convert -w 64 -h 64 -o $@ $^

icons/gtk/iceape.png icons/gtk/default.png icons/gtk/main-window.png:: $(srcdir)/iceape_icon_plain.svg
	rsvg-convert -w 32 -h 32 -o $@ $^

icons/gtk/default16.png icons/gtk/main-window16.png:: $(srcdir)/iceape_icon_plain.svg
	rsvg-convert -w 16 -h 16 -o $@ $^

icons/gtk/default48.png icons/gtk/main-window48.png:: $(srcdir)/iceape_icon_plain.svg
	rsvg-convert -w 48 -h 48 -o $@ $^

$(foreach file,$(ALL_FILES),$(if $(wildcard $(topsrcdir)/suite/branding/$(file)),$(file))):: %: $(topsrcdir)/suite/branding/%
	$(INSTALL) $^ $(dir $@)

libs:: $(addprefix icons/gtk/,$(DESKTOP_ICON_FILES))
	$(INSTALL) $^ $(DIST)/bin/chrome/icons/default

GARBAGE	+= $(addprefix $(DIST)/bin/chrome/icons/default/,$(DESKTOP_ICON_FILES))

GARBAGE_DIRS += content icons locales # The latter is created by the build system with no reason

ICONS = $(foreach size,16 32 48 64 128,iceape$(size).png)
$(ICONS):: $(srcdir)/iceape_icon_plain.svg
	rsvg-convert -w $(@:iceape%.png=%) -h $(@:iceape%.png=%) -o $@ $^

$(addprefix install-, $(ICONS)): install-%: %
	[ -d $(DESTDIR)/usr/share/icons/hicolor/$(@:install-iceape%.png=%)x$(@:install-iceape%.png=%)/apps ] || mkdir -p $(DESTDIR)/usr/share/icons/hicolor/$(@:install-iceape%.png=%)x$(@:install-iceape%.png=%)/apps
	install $(IFLAGS1) $^ $(DESTDIR)/usr/share/icons/hicolor/$(@:install-iceape%.png=%)x$(@:install-iceape%.png=%)/apps/iceape.png

install:: $(addprefix install-, $(ICONS))
	[ -d $(DESTDIR)/usr/share/icons/hicolor/scalable/apps ] || mkdir -p $(DESTDIR)/usr/share/icons/hicolor/scalable/apps
	install $(IFLAGS1) $(srcdir)/iceape_icon_plain.svg $(DESTDIR)/usr/share/icons/hicolor/scalable/apps/iceape.svg

GARBAGE += $(ICONS)

EXTRA_COMPONENTS := aboutIce.js

include $(topsrcdir)/config/rules.mk
