# iceape.SlackBuild for Slackware 14.2 and derivatives

The following script is based (_forked_) on Pat [seamonkey.SlackBuild](http://ftp.slackware.com/pub/slackware/slackware64-14.2/source/xap/seamonkey/seamonkey.SlackBuild).

## Build

* Clone this repository:

```bash
git clone https://gitlab.com/icaroperseo/iceape-slackbuild.git
cd iceape-slackbuild
```

* Download the `seamonkey-2.40.source.tar.xz` file from any official Slackware mirror, for example:

```bash
wget -c http://ftp.slackware.com/pub/slackware/slackware64-14.2/source/xap/seamonkey/seamonkey-2.40.source.tar.xz
```

* Compile as root:

```bash
chmod +x iceape.SlackBuild
su -
./iceape.SlackBuild
```

* Done!

## Known issues: oxygen-gtk

If Iceape unexpectedly quits when you try to click to any menu (main or pop-up) then try to run it from the terminal to get some output that describes the issue. If the output is similar to the following:

```text
(process:2867): GLib-CRITICAL **: g_slice_set_config: assertion 'sys_page_size == 0' failed
[2867] ###!!! ABORT: X_CreatePixmap: BadAlloc (insufficient resources for operation); 4 requests ago: file /home/buildbot/buildslave/linux64/build/mozilla/toolkit/xre/nsX11ErrorHandler.cpp, line 157
[2867] ###!!! ABORT: X_CreatePixmap: BadAlloc (insufficient resources for operation); 4 requests ago: file /home/buildbot/buildslave/linux64/build/mozilla/toolkit/xre/nsX11ErrorHandler.cpp, line 157
```

then, try what is suggested [here](https://bugzilla.mozilla.org/show_bug.cgi?id=1062145#c3). For example, as root:

* For Slackware64:

```bash
ln -s /usr/lib64/iceape-2.40/iceape /usr/bin/seamonkey
```

and then edit/create the `iceape.desktop` file replacing _iceape_ with _seamonkey_ bin path.

### Workaround 2

Run it from the _terminal_/_krunner_ as follows:

```bash
OXYGEN_APPLICATION_NAME_OVERRIDE=xulrunner iceape
```

### Workaround 3 (the best option right now)

Create the `/usr/local/bin/iceape` file adding the following lines, as root:

```bash
export OXYGEN_APPLICATION_NAME_OVERRIDE=xulrunner
exec /usr/bin/iceape $@
```

Next:

```bash
chmod +x /usr/local/bin/iceape
```

**Sources:**

* [Crash when opening contextual menus on Linux x64](https://bugzilla.mozilla.org/show_bug.cgi?id=1062145)
* [Mozilla/XUL application list is missing some applications, leading to crashes](https://bugs.kde.org/show_bug.cgi?id=341181)
